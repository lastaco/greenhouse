import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class LightingPage extends StatefulWidget {
  const LightingPage({
    Key? key,
  }) : super(key: key);

  @override
  State<LightingPage> createState() => _LightingPageState();
}

class _LightingPageState extends State<LightingPage> {
  @override
  void initState() {
    super.initState();
  }

  final _controller = PageController();

  bool lighting = false;
  bool thermostat = false;
  bool watering = false;
  bool humidity = false;
  bool wind = false;
  int temp = 18;
  int vol = 55;
  double _currentSliderValue = 20;
  double _currentSliderValue2 = 60;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double topSafeArea = MediaQuery.of(context).padding.top;
    double bottomSafeArea = MediaQuery.of(context).padding.bottom;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          'Освітленняя',
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.sunny,
                      size: 24,
                      color:  Colors.black,
                    ),
                    Transform.scale(
                      scale: 0.8,
                      child: CupertinoSwitch(
                        trackColor:
                        Colors.green.withOpacity(0.2),
                        activeColor:
                        Colors.green,
                        value: true,
                        onChanged: (value) async {

                        },
                      ),
                    ),
                  ],),
                const SizedBox(
                  height: 15,
                ),
                Icon(Icons.light, color: Colors.black, size: 100,),


                const SizedBox(height: 30,),
                Text(
                  'Теплота світла: ${_currentSliderValue.round()}%',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: lighting
                        ? Colors.white
                        : Colors.black,
                  ),
                ),
                Slider(
                  value: _currentSliderValue,
                  max: 100,
                  divisions: 20,
                  activeColor: Colors.green,
                  thumbColor: Colors.green,
                  label: _currentSliderValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue = value;
                    });
                  },
                ),
                const SizedBox(height: 25,),
                Text(
                  'Яскравість: ${_currentSliderValue2.round()}%',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: lighting
                        ? Colors.white
                        : Colors.black,
                  ),
                ),
                Slider(
                  value: _currentSliderValue2,
                  max: 100,
                  divisions: 20,
                  activeColor: Colors.green,
                  thumbColor: Colors.green,
                  label: _currentSliderValue2.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue2 = value;
                    });
                  },
                ),
                const SizedBox(height: 30,),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
