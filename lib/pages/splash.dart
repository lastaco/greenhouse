import 'dart:async';
import 'dart:io';


import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'package:location/location.dart';


import '../../data/user_repository.dart';
import '../../main.dart';
import '../bloc/user_bloc.dart';
import 'main_page.dart';


class SplashLoaderPage extends StatefulWidget {
  const SplashLoaderPage({
    Key? key,
    this.category = false,
  }) : super(key: key);
  final bool category;

  @override
  State<SplashLoaderPage> createState() => _SplashLoaderPageState();
}

var bloc =
    BlocProvider.of<UserBloc>(App.navigatorKey.currentState!.context).state;

class _SplashLoaderPageState extends State<SplashLoaderPage> {
  void loadMain() async {
    if(BlocProvider.of<UserBloc>(context).state.temperature != ""){

      timer?.cancel();
      await precacheImage(
        CachedNetworkImageProvider(BlocProvider.of<UserBloc>(context).state.image),
        context,
      );

      Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) => const HomePage(


          ),
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
        ),
        ModalRoute.withName('/home'),

      );


    }

  }



  Timer? timer;



  @override
  void initState() {
    initializeDateFormatting();
    BlocProvider.of<UserBloc>(App.navigatorKey.currentState!.context).loadProfile();
    timer = Timer.periodic(
        const Duration(milliseconds: 1), (Timer t) => loadMain());


    super.initState();

  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      color: const Color(0xFF1573FF),

    );
  }
}
