import 'dart:math';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:greenhouse/pages/lighting.dart';
import 'package:intl/intl.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../data/user_repository.dart';

class GreenHousePage extends StatefulWidget {
  const GreenHousePage({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;

  @override
  State<GreenHousePage> createState() => _GreenHousePageState();
}

class _GreenHousePageState extends State<GreenHousePage> {
  @override
  void initState() {
    super.initState();
  }
  final List<String> plants = [
    'Томати',
    'Огірки',
    'Перець',
    'Салат',
    'Базилік',
    'Кабачки',
    'Морква',
    'Редис',
    'Капуста',
    'Цибуля',
    'Часник',
    'Петрушка',
    'Кріп',
    'Баклажани',
    'Гарбуз',
    'Картопля',
    'Шпинат',
    'Броколі',
    'Цукіні',
    'Кінза',
    'Рукола',
    'Селера',
    'Мангольд',
    'Крес-салат',
    'Червоний буряк',
    'Квасоля',
    'Горох',
    'Сочевиця',
    'Гірчиця',
    'Ріпа'
  ];


  String? selectedPlant;





  void showCustomAlertDialog(BuildContext context, String text) {
    showDialog(
      context: context,
      barrierDismissible: true,  // Позволяет закрыть диалог при нажатии вне его
      builder: (BuildContext context) {
        return AlertDialog(
          insetPadding:  EdgeInsets.all(25),
          actionsPadding: EdgeInsets.zero,
          buttonPadding: EdgeInsets.zero,
          contentPadding: EdgeInsets.only(left: 24, right: 24, top: 24),

          title: Text("Рекомендації Штучного Інтелекту"),
          content: SingleChildScrollView(
            child: RichText(

              text: TextSpan(
                style: TextStyle(color: Colors.black),
                children: _parseText(text),
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("OK", style: TextStyle(color: Colors.green),),
            ),
          ],
        );
      },
    );
  }


  List<TextSpan> _parseText(String text) {
    List<TextSpan> spans = [];
    RegExp regExp = RegExp(r'(\*\*)(.*?)(\*\*)');
    Iterable<Match> matches = regExp.allMatches(text);

    int lastEnd = 0;
    for (var match in matches) {
      if (match.start > lastEnd) {
        spans.add(TextSpan(
          text: text.substring(lastEnd, match.start),
        ));
      }
      spans.add(TextSpan(
        text: match.group(2),  // Текст между звездами
        style: TextStyle(fontWeight: FontWeight.bold),
      ));
      lastEnd = match.end;
    }

    if (lastEnd < text.length) {
      spans.add(TextSpan(text: text.substring(lastEnd)));
    }

    return spans;
  }

  final _controller = PageController();

  bool lighting = false;
  bool thermostat = false;
  bool thermostatG = false;
  bool watering = false;
  bool humidity = false;
  bool humidityG = false;
  bool wind = false;
  bool co2Bool = false;
  int temp = 18;
  int tempG = 22;
  int vol = 55;
  int co2 = 0;
  int volG = 70;
  bool load = false;


  final random = Random();
  final now = DateTime.now();
 late final days = List.generate(7, (index) => now.subtract(Duration(days: index))).reversed.toList();
  late final data = days.map((day) => 1000 + random.nextInt(151).toDouble()).toList();
  late  final data2 = days.map((day) => 5 + random.nextInt(3).toDouble()).toList();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    double topSafeArea = MediaQuery.of(context).padding.top;
    double bottomSafeArea = MediaQuery.of(context).padding.bottom;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          widget.title,
          style: const TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [

              const SizedBox(
                height: 15,
              ),
              Column(
                children: [
                  SizedBox(
                    height: 200,
                    child: PageView(
                      controller: _controller,
                      children: [
                        Row(
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: width - 40,
                              height: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: AssetImage(
                                        'assets/8x12-greenhouse-palram-620x450.jpg'),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: width - 40,
                              height: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: const AssetImage(
                                        'assets/Untitled_design_17_1600x.webp'),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: width - 40,
                              height: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: AssetImage(
                                        'assets/Victory_-10-ft.-x-12-ft.-Orangery-Chalet-Greenhouse---Palram-Canopia-Canopia-by-Palram-1642863298_1200x.webp'),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: width - 40,
                              height: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  image: const DecorationImage(
                                    image: const AssetImage(
                                        'assets/Gotham-Greens-greenhouse.jpg'),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  SmoothPageIndicator(
                      controller: _controller,
                      count: 4,
                      effect: const SwapEffect(
                        activeDotColor: Colors.green,
                        dotColor: Color(0xFFE1E5EA),
                        dotHeight: 8,
                        dotWidth: 8,
                        spacing: 6,
                        //verticalOffset: 50,
                      )),
                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color:  Colors.green, width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 15,vertical: 2,),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.warning_amber_rounded,
                                      size: 24,
                                      color: Colors.red,
                                    ),
                                    const SizedBox(width: 8),
                                    Column(
                                      children: const [
                                        SizedBox(height: 3),
                                        Text(
                                          'Аварії',
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Text(
                                  'Відстутні',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.green,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Divider( color: Colors.green,),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.error_outline_rounded,
                                      size: 24,
                                      color: Colors.red,
                                    ),
                                    const SizedBox(width: 8),
                                    Column(
                                      children: const [
                                        SizedBox(height: 3),
                                        Text(
                                          'Помилки',
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Text(
                                  'Відстутні',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.green,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: [
                        Text(
                          'Вага рослин з тензодатчиків',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color:  Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),

                  SizedBox(
                    height: 200,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: LineChart(
                        LineChartData(
                          minX: 0,
                          maxX: days.length - 1,
                          minY: 0,
                          maxY: 1500,
                          titlesData: FlTitlesData(

                            leftTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                reservedSize: 40,
                                getTitlesWidget: (value, meta) {
                                  return Text(
                                    '${value.toInt()} г',
                                    style: const TextStyle(fontSize: 12),
                                  );
                                },
                              ),
                            ),
                            rightTitles: AxisTitles(),
                            topTitles: AxisTitles(),
                            bottomTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                getTitlesWidget: (value, meta) {
                                  if (value.toInt() >= 0 && value.toInt() < days.length) {
                                    final date = days[value.toInt()];
                                    return Column(
                                      children: [
                                        const SizedBox(height: 5,),
                                        Text(
                                          DateFormat('dd.MM').format(date),
                                          style: const TextStyle(fontSize: 12),
                                        ),
                                      ],
                                    );
                                  }
                                  return const SizedBox();
                                },
                              ),
                            ),
                          ),
                          gridData: FlGridData(show: true),
                          borderData: FlBorderData(show: true),

                          lineBarsData: [
                            LineChartBarData(
                              spots: List.generate(
                                data.length,
                                    (index) => FlSpot(index.toDouble(), data[index]),
                              ),
                              isCurved: true,
                              color: Colors.green,
                              barWidth: 4,
                              belowBarData: BarAreaData(
                                show: true,
                                color: Colors.green.withOpacity(0.2),
                              ),
                              dotData: FlDotData(show: true),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),const SizedBox(
                    height: 15,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      children: [
                        Text(
                          'Кислотність ґрунту',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color:  Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    height: 200,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: LineChart(
                        LineChartData(
                          minX: 0,
                          maxX: days.length - 1,
                          minY: 0,
                          maxY: 14,
                          titlesData: FlTitlesData(

                            leftTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                reservedSize: 40,
                                getTitlesWidget: (value, meta) {
                                  return Text(
                                    '${value.toInt()} ph',
                                    style: const TextStyle(fontSize: 12),
                                  );
                                },
                              ),
                            ),
                            rightTitles: AxisTitles(),
                            topTitles: AxisTitles(),
                            bottomTitles: AxisTitles(
                              sideTitles: SideTitles(
                                showTitles: true,
                                getTitlesWidget: (value, meta) {
                                  if (value.toInt() >= 0 && value.toInt() < days.length) {
                                    final date = days[value.toInt()];
                                    return Column(
                                      children: [
                                        const SizedBox(height: 5,),
                                        Text(
                                          DateFormat('dd.MM').format(date),
                                          style: const TextStyle(fontSize: 12),
                                        ),
                                      ],
                                    );
                                  }
                                  return const SizedBox();
                                },
                              ),
                            ),
                          ),
                          gridData: FlGridData(show: true),
                          borderData: FlBorderData(show: true),

                          lineBarsData: [
                            LineChartBarData(
                              spots: List.generate(
                                data.length,
                                    (index) => FlSpot(index.toDouble(), data2[index]),
                              ),
                              isCurved: true,
                              color: Colors.green,
                              barWidth: 4,
                              belowBarData: BarAreaData(
                                show: true,
                                color: Colors.green.withOpacity(0.2),
                              ),
                              dotData: FlDotData(show: true),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                               if(lighting){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) => const LightingPage()),
                                 );
                               }
                              },
                              borderRadius: BorderRadius.circular(15),
                              child: Container(
                                width: (width - 70+15) / 2,
                                height: (width - 70+15) / 2,
                                decoration: BoxDecoration(
                                  color: lighting
                                      ? Colors.green
                                      : Colors.transparent,
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(
                                    width: 0.5,
                                    color: Colors.grey,
                                  ),
                                ),
                                padding: const EdgeInsets.all(15),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Icon(
                                          Icons.sunny,
                                          size: 24,
                                          color: lighting
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                        Transform.scale(
                                          scale: 0.7,
                                          child: CupertinoSwitch(
                                            trackColor:
                                                Colors.green.withOpacity(0.2),
                                            activeColor:
                                                Colors.black.withOpacity(0.2),
                                            value: lighting,
                                            onChanged: (value) async {
                                              setState(() {
                                                lighting = !lighting;
                                              });
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Освітленя',
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            color: lighting
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: watering
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.water_drop_sharp,
                                        size: 24,
                                        color: watering
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                          Colors.green.withOpacity(0.2),
                                          activeColor:
                                          Colors.black.withOpacity(0.2),
                                          value: watering,
                                          onChanged: (value) async {
                                            setState(() {
                                              watering = !watering;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Полив',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: watering
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),

                          ],
                        ),
                        const SizedBox(height: 15,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: wind
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.air,
                                        size: 24,
                                        color: wind
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                          Colors.green.withOpacity(0.2),
                                          activeColor:
                                          Colors.black.withOpacity(0.2),
                                          value: wind,
                                          onChanged: (value) async {
                                            setState(() {
                                              wind = !wind;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Провітрення',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: wind
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: co2Bool
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.bubble_chart,
                                        size: 24,
                                        color: co2Bool
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                          Colors.green.withOpacity(0.2),
                                          activeColor:
                                          Colors.black.withOpacity(0.2),
                                          value: co2Bool,
                                          onChanged: (value) async {
                                            setState(() {
                                              co2Bool = !co2Bool;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Насичення CO2',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: co2Bool
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  !co2Bool
                                      ? const SizedBox()
                                      : Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            co2--;
                                          });
                                        },
                                        icon: Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                      Text(
                                        '  $co2%  ',
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,

                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            co2++;
                                          });
                                        },
                                        icon: Icon(Icons.add,                                                color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 15,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: thermostat
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.thermostat,
                                        size: 24,
                                        color: thermostat
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                          Colors.green.withOpacity(0.2),
                                          activeColor:
                                          Colors.black.withOpacity(0.2),
                                          value: thermostat,
                                          onChanged: (value) async {
                                            setState(() {
                                              thermostat = !thermostat;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Температура \nповітря',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: thermostat
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  !thermostat
                                      ? const SizedBox()
                                      : Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            temp--;
                                          });
                                        },
                                        icon: Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                      Text(
                                        '  $temp  ',
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,

                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            temp++;
                                          });
                                        },
                                        icon: Icon(Icons.add,                                                color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: humidity
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.water_rounded,
                                        size: 24,
                                        color: humidity
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                              Colors.green.withOpacity(0.2),
                                          activeColor:
                                              Colors.black.withOpacity(0.2),
                                          value: humidity,
                                          onChanged: (value) async {
                                            setState(() {
                                              humidity = !humidity;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Вологість \nповітря',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: humidity
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  !humidity
                                      ? const SizedBox()
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  vol--;
                                                });
                                              },
                                              icon: Icon(
                                                Icons.remove,
                                                color: Colors.white,
                                              ),
                                              padding: EdgeInsets.zero,
                                              constraints: BoxConstraints(),
                                            ),
                                            Text(
                                              '  $vol%  ',
                                              style: TextStyle(
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,

                                              ),
                                            ),
                                            IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  vol++;
                                                });
                                              },
                                              icon: Icon(Icons.add,                                                color: Colors.white,
                                              ),
                                              padding: EdgeInsets.zero,
                                              constraints: BoxConstraints(),
                                            ),
                                          ],
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 15,),Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: thermostatG
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.thermostat,
                                        size: 24,
                                        color: thermostatG
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                          Colors.green.withOpacity(0.2),
                                          activeColor:
                                          Colors.black.withOpacity(0.2),
                                          value: thermostatG,
                                          onChanged: (value) async {
                                            setState(() {
                                              thermostatG = !thermostatG;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Температура \nповітря',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: thermostatG
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  !thermostatG
                                      ? const SizedBox()
                                      : Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            tempG--;
                                          });
                                        },
                                        icon: Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                      Text(
                                        '  $tempG  ',
                                        style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,

                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          setState(() {
                                            tempG++;
                                          });
                                        },
                                        icon: Icon(Icons.add,                                                color: Colors.white,
                                        ),
                                        padding: EdgeInsets.zero,
                                        constraints: BoxConstraints(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: (width - 70+15) / 2,
                              height: (width - 70+15) / 2,
                              decoration: BoxDecoration(
                                color: humidityG
                                    ? Colors.green
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(15),
                                border: Border.all(
                                  width: 0.5,
                                  color: Colors.grey,
                                ),
                              ),
                              padding: const EdgeInsets.all(15),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Icon(
                                        Icons.water_rounded,
                                        size: 24,
                                        color: humidityG
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                      Transform.scale(
                                        scale: 0.7,
                                        child: CupertinoSwitch(
                                          trackColor:
                                              Colors.green.withOpacity(0.2),
                                          activeColor:
                                              Colors.black.withOpacity(0.2),
                                          value: humidityG,
                                          onChanged: (value) async {
                                            setState(() {
                                              humidityG = !humidityG;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Вологість \nґрунту',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: humidityG
                                              ? Colors.white
                                              : Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  !humidityG
                                      ? const SizedBox()
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  volG--;
                                                });
                                              },
                                              icon: Icon(
                                                Icons.remove,
                                                color: Colors.white,
                                              ),
                                              padding: EdgeInsets.zero,
                                              constraints: BoxConstraints(),
                                            ),
                                            Text(
                                              '  $volG%  ',
                                              style: TextStyle(
                                                fontSize: 30,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,

                                              ),
                                            ),
                                            IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  volG++;
                                                });
                                              },
                                              icon: Icon(Icons.add,                                                color: Colors.white,
                                              ),
                                              padding: EdgeInsets.zero,
                                              constraints: BoxConstraints(),
                                            ),
                                          ],
                                        ),
                                ],
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                  const SizedBox(height: 15,),
                  Container(
                    height: 56,
                    width: MediaQuery.of(context).size.width - 40,
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: DropdownButton<String>(

                      hint: Text(selectedPlant ?? 'Оберіть рослину'),
                      value: selectedPlant,
                      isExpanded: true,
                      underline: SizedBox(),
                      items: plants.map((String plant) {
                        return DropdownMenuItem<String>(
                          value: plant,
                          child: Text(plant),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedPlant = newValue;
                        });
                      },
                    ),
                  ),
                  const SizedBox(height: 15,),
                  TextButton(
                    onPressed: ()  async {
                     setState(() {
                       load = true;
                     });
                     var response = await getAiInfo('В мене є теплиця. В якій температура повітря $temp градусів, вологість повітря $vol%, температура ґрунту $tempG і вологість ґрунту $volG. Кислотність ґрунту ${data2.last} ph. Також вага рослин з 8 тензодатчиків ${data.last} грами. В мене є освітлення, можу насичувати повітря СО2, також є винтиляція. Тип ґрунту чорнозем. Автоматичний капельний полив. Я вирощую ${selectedPlant.toString()}. Дай мені якісь рекомендації та проведи аналіз, побільше тексту.');
                     showCustomAlertDialog(context,response!.data['candidates'][0]['content']['parts'][0]['text'] );
                     setState(() {
                       load = false;
                     });
                    },
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(load ? Colors.grey.withOpacity(0.4) :Colors.green),
                      padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 16.0)),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),
                      minimumSize: MaterialStateProperty.all(Size(width-40, 56)),

                    ),
                    child: load ? CupertinoActivityIndicator(radius: 10,): Text(
                      'Рекомендації ШІ',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white, // Белый текст
                      ),
                    ),
                  )
,
                  const SizedBox(height: 15,),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
