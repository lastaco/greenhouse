part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

class LoadBalanceEvent extends UserEvent {
  @override
  List<Object?> get props => [];
}

class LoadValidationEvent extends UserEvent {
  @override
  List<Object?> get props => [];
}

class LoadProfileEvent extends UserEvent {
  @override
  List<Object?> get props => [];
}

class RemoveProfileEvent extends UserEvent {
  @override
  List<Object?> get props => [];
}

class SaveTokenEvent extends UserEvent {
  final String token;

  const SaveTokenEvent(this.token);

  @override
  List<Object?> get props => [token];
}