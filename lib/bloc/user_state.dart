part of 'user_bloc.dart';

class UserState extends Equatable {
  static const Map<String, List<String>> discountss = {
    '': ['']
  };
  static const List orders = [];

  const UserState._({
    this.date = "",
    this.humidity = "",
    this.temperature = "",
    this.windSpeed = "",
    this.image = "",
    this.name = "",
    this.city = "",

  });

  final String date;
  final String humidity;
  final String temperature;
  final String image;
  final String windSpeed;
  final String name;
  final String city;


  const UserState.update({
    required this.date,
    required this.humidity,
    required this.temperature,
    required this.windSpeed,
    required this.image,
    required this.name,
    required this.city,

  });

  const UserState.clean() : this._();

  @override
  List<Object> get props => [
    date,
    humidity,
    temperature,
    windSpeed,
    image,
    name,
    city,

  ];
}
