import 'dart:developer';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import '../../data/user_repository.dart';
import '../../main.dart';
import 'package:intl/intl.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const UserState._()) {
    on<LoadProfileEvent>((event, emit) async {

      var locationGet = Location();
      LocationData position = await locationGet.getLocation();
      var response = await getWeather(position.latitude, position.longitude);
      var response2 = await getCity(position.latitude, position.longitude);
      DateTime now =  DateTime.now();
      String date = DateFormat('d MMMM', 'uk').format(now);
      String date2 = DateFormat('E', 'uk').format(now);
      emit(UserState.update(
        date: '$date, ${date2.capitalize()}',
        humidity: response.data['main']['humidity'].toString(),
        temperature: response.data['main']['temp'].toString(),
        windSpeed: response.data['wind']['speed'].toString(),
        image: "https://openweathermap.org/img/wn/${response.data['weather'][0]['icon']}@2x.png",
        name: response.data['weather'][0]['description'].toString().capitalize(),
        city: response2.data['city'].toString(),
      ));

    });

    on<RemoveProfileEvent>((event, emit) async {
      emit(const UserState.update(
        date: "",
        humidity: "",
        temperature: "",
        windSpeed: "",
        image: "",
        name: "",
        city: "",
      ));
    });
    on<SaveTokenEvent>((event, emit) {
      emit(UserState.update(
        date: state.date,
        humidity: state.humidity,
        temperature: state.temperature,
        windSpeed: state.windSpeed,
        image: state.image,
        name: state.name,
        city: state.city,
      ));
    });
  }

  void loadProfile() async {
    add(LoadProfileEvent());
  }

  void removeProfile() {
    add(RemoveProfileEvent());
  }

  void saveToken(String token) {
    add(SaveTokenEvent(token));
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  }
}