import 'package:dio/dio.dart';

Future<dynamic> getAiInfo(text) async {
 var response = await Dio().post(
    'https://generativelanguage.googleapis.com/v1beta/models/gemini-1.5-flash-latest:generateContent?key=AIzaSyBmE3_FZ9PQT0x3OFfz0pnHmwwLtLID8u8',
   data: {
     "contents": [
       {
         "parts": [
           {
             "text": text,  // Запрос от пользователя
           }
         ]
       }
     ]
   },
   options: Options(
     headers: {
       'Content-Type': 'application/json',
     },
   )


  );
  return response;
}

Future<dynamic> getWeather(lat, lng) async {
  const String apiKey = 'bd5e378503939ddaee76f12ad7a97608';
  final String url = 'https://api.openweathermap.org/data/2.5/weather';
  var response = await Dio().get(
    url,
    queryParameters: {
      'lat': lat,
      'lon': lng,
      'appid': apiKey,
      'units': 'metric',
      'lang': 'ua',
    },
  );
  return response;
}



Future<dynamic> getCity(lat, lng) async {
  var response = await Dio().get(
    "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=$lat&longitude=$lng&localityLanguage=uk",
  );
  return response;
}
