import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:greenhouse/pages/main_page.dart';
import 'package:greenhouse/pages/splash.dart';

import 'bloc/user_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    const App(),
  );
}

class App extends StatelessWidget {
  const App({
    Key? key,
    this.index = 1,
    this.locale = '',
  }) : super(key: key);
  final int index;
  final String locale;

  static String ref = "";
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return RepositoryProvider.value(
        value: null,
        child: MultiBlocProvider(
            providers: [
              BlocProvider<UserBloc>(
                create: (context) => UserBloc(),
              ),


            ],
            child: AppView(

            )));
  }
}


class AppView extends StatefulWidget {
  const AppView({
    Key? key,
  }) : super(key: key);


  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  @override


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Моя теплиця',
      navigatorKey: App.navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        sliderTheme: const SliderThemeData(
          showValueIndicator: ShowValueIndicator.never,
        ),
      ),
      initialRoute: '/splash',
      routes: {

        '/splash': (context) => const SplashLoaderPage(),
        '/home': (context) => const HomePage(),

      },

    );
  }
}





